document.addEventListener("DOMContentLoaded", function() {
    function select(selector) {
        return document.querySelector(selector);
    }

    var input = select("#inputFile");
    var image = document.createElement("img")
    var p = select(".previewPlace");

    input.addEventListener("change", function() {
        if (this.files[0] === 0) {
            return false;
        }
        var reader = new FileReader()
        var file = this.files[0];
        p.innerText = "";
        reader.onload = function(event) {
            p.appendChild(image)
            image.src = event.target.result;
            image.className = "imagePreviewed"
        }

        reader.readAsDataURL(file)
    })
})